var products = [];

const getProducts = () => {
    fetch("http://localhost:3000/products")
    .then(res => res.json())
    .then(data => {
        products = data;
        console.log(products);
        displayProd();
    })
};

const displayProd = () => {
    products.forEach(product => {
        document.getElementById("products").innerHTML += `
        <div class=card><br>
        <h2>${product.title}</h2><br>
        <h4>${product.brand}</h4>
        <img src="${product.thumbnail}" alt = "${product.title}"></img>
        <p>${product.description}</p><br>
        <h4>$ ${product.price}</h4><br>
        <button type="button" class="btn btn-primary" onclick="addsaveforlater(${product.id})">Add to saveforlater</button><br>
        </div>`
    });
}

var favs = [];

const getsaveforlater = () => {
    fetch("http://localhost:3000/saveforLater")
    .then(res => res.json())
    .then(data => {
        favs = data;
        console.log(favs);
        displayFavs();
    })
}

displayFavs = () => {
    favs.forEach(fav => {
        document.getElementById("saveforlater").innerHTML += `
        <div class=card><br>
        <h2>${fav.title}</h2><br>
        <h4>${fav.brand}</h4>
        <img src="${fav.thumbnail}" alt = "${fav.title}"></img>
        <p>${fav.description}</p><br>
        <h4>$ ${fav.price}</h4><br>
        <button type="button" class="btn btn-primary" onclick="deleteSaveForLater(${fav.id})">Delete Item</button><br>
        </div>`
    });
}

const addsaveforlater = (favId) => {
	console.log("Added to favourite");
	let obj = getProductById(favId)
	favs.push(obj);
	console.log(favs);

    fetch("http://localhost:3000/saveforLater",
    {
        method:'POST',
        body: JSON.stringify(obj),
        headers:
        {
            "Content-Type":"application/json"
        }
    })
    .then(res => {res.json()
    }
    )
    .then(console.log(obj))
}

const getProductById = (prodId) => {
    for(let i=0;i<products.length;i++) {
        if(prodId == products[i].id) {
            console.log(products[i]);
            return products[i]
        }
    }
   }

const deleteSaveForLater = (id) => {
    fetch(`http://localhost:3000/saveforLater/${id}`,
    {
        method:'DELETE',
        headers:
        {
            "Content-Type":"application/json"
        }
    })
    .then(console.log(`ID - ${id} Deleted`))
}


getProducts();
getsaveforlater();